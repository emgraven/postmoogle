module gitlab.com/etke.cc/postmoogle

go 1.18

require (
	git.sr.ht/~xn/cache/v2 v2.0.0
	github.com/emersion/go-smtp v0.15.0
	github.com/getsentry/sentry-go v0.13.0
	github.com/jhillyerd/enmime v0.10.0
	github.com/lib/pq v1.10.6
	github.com/mattn/go-sqlite3 v1.14.14
	gitlab.com/etke.cc/go/env v1.0.0
	gitlab.com/etke.cc/go/logger v1.1.0
	gitlab.com/etke.cc/linkpearl v0.0.0-20220831124140-598117f26c77
	golang.org/x/net v0.0.0-20220826154423-83b083e8dc8b
	maunium.net/go/mautrix v0.12.0
)

require (
	github.com/cention-sany/utf7 v0.0.0-20170124080048-26cad61bd60a // indirect
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21 // indirect
	github.com/gogs/chardet v0.0.0-20191104214054-4b6791f73a28 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/jaytaylor/html2text v0.0.0-20200412013138-3577fbdbcff7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/rs/zerolog v1.28.0 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	github.com/tidwall/gjson v1.14.3 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/tidwall/sjson v1.2.5 // indirect
	github.com/yuin/goldmark v1.4.12 // indirect
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90 // indirect
	golang.org/x/sys v0.0.0-20220829200755-d48e67d00261 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	maunium.net/go/maulogger/v2 v2.3.2 // indirect
)
